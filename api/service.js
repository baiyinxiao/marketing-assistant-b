/**
 * @version 3.0.4
 * @Author lu-ch
 * @Email webwork.s@qq.com
 * 文档: https://www.quanzhan.co/luch-request/
 * github: https://github.com/lei-mu/luch-request
 * DCloud: http://ext.dcloud.net.cn/plugin?id=392
 * HBuilderX: beat-2.7.14 alpha-2.8.0
 */
import Request from '@/utils/luch-request/index.js'
import {get} from '@/utils/storage/storage.js'
import peojectConfig from './config.js'
const getPhoneStorage = () => {
    let phone = ''
    try {
        phone = get('phone') ? get('phone') : ''
    } catch (e) {
    }
    return phone
}
let goLoginStatus = false
	
const http = new Request()
http.setConfig((config) => { /* 设置全局配置 */
   config.baseURL =peojectConfig.url.httpsUrl
  config.header = {
	  contentType: "application/json;charset-utf-8",
    ...config.header
  }
  return config
})


http.interceptors.request.use((config) => { /* 请求之前拦截器。可以使用async await 做异步操作 */
  config.header = {
    ...config.header,
	phone: getPhoneStorage()
  }
  /*
 if (!token) { // 如果token不存在，return Promise.reject(config) 会取消本次请求
   return Promise.reject(config)
 }
 */
uni.showLoading({
	mask:true
})
  return config
}, (config) => {
  return Promise.reject(config)
})


http.interceptors.response.use(async (response) => { /* 请求之后拦截器。可以使用async await 做异步操作  */
uni.hideLoading()
  if(response.data.code ==-1&&!goLoginStatus){
	  uni.showToast({
	  	title:'登录失效，请重新登录',
	  	icon:'none'
	  })
	  uni.navigateTo({
	  	url:'/pages/login/login?type=1'
	  })
	  goLoginStatus=true
	  return Promise.reject(response.data)
  }else if(response.data.code ==500){
	  uni.showToast({
          title: "服务器异常,请稍后再试!",
          icon: 'none'
      })
	  return Promise.reject(response.data)
  }else if(response.data.code ==0){
	  uni.showToast({
          title: response.data.msg,
          icon: 'none'
      })
	  return Promise.reject(response.data)
  }
  goLoginStatus=false
  return response.data
}, (response) => { // 请求错误做点什么。可以使用async await 做异步操作
  console.log(response)
  uni.showToast({
  	title:'服务器异常',
	icon:'none'
  })
  uni.hideLoading()
  return Promise.reject(response)
})


export {
	http
}
