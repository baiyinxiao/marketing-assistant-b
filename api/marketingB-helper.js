/**
 * 营销助手管理端相关接口
 */
import {http} from '@/api/service.js'

/**
 * 登陆
 */
//登录
export const login = (params) => {
	return http.post('/api/login', params)
};


/**
 * 核销
 */
//核销页面
export const writeoffDetail = (params) => {
	return http.post('/api/writeoff/detail', params)
};

//核销
export const writeoffWriteOff = (params) => {
	return http.post('/api/writeoff/writeOff', params)
};

//核销tab数据
export const writeoffTabList = (params) => {
	return http.post('/api/writeoff/tabList', params)
};

//核销列表数据
export const writeoffList = (params) => {
	return http.post('/api/writeoff/list', params)
};

//核销记录详情信息
export const writeoffLogInfo = (params) => {
	return http.post('/api/writeoff/logInfo', params)
};



/**
 * 推广任务
 */
//任务列表
export const promotionTaskList = (params) => {
	return http.post('/api/promotionTask/list', params)
};

//查看任务
export const promotionTaskInfo = (params) => {
	return http.get('/api/promotionTask/taskInfo/'+ params)
};

//分享活动
export const promotionTaskQrcode = (params) => {
	return http.post('/api/promotionTask/qrcode',params)
};

//任务统计
export const promotionTaskStatistics = (params) => {
	return http.get('/api/promotionTask/statistics')
};

/**
 * 活动数据
 */
//活动数据
export const getActityInfo = (params) => {
	return http.get('/api/records/getActityInfo/'+ params)
};

//浏览客户列表
export const recordsList = (params) => {
	return http.post('/api/records/list', params)
};

//秒杀活动已购买客户列表
export const getPurchasedCustomers = (params) => {
	return http.post('/api/records/getPurchasedCustomers', params)
};

//集客活动已参与客户列表
export const getParticipatingCustomers = (params) => {
	return http.post('/api/records/getParticipatingCustomers', params)
};

//同行活动发起客户列表
export const selectInvitedUsersList = (params) => {
	return http.post('/api/records/selectInvitedUsersList', params)
};
//同行活动参与客户列表
export const selectPeerParticipating = (params) => {
	return http.post('/api/records/selectPeerParticipating', params)
};
//同行活动获奖客户列表
export const getAwardWinningUsers = (params) => {
	return http.post('/api/records/getAwardWinningUsers', params)
};

//抽奖活动参与用户列表
export const getParticipatingUsers = (params) => {
	return http.post('/api/records/getParticipatingUsers', params)
};

//抽奖活动获奖用户列表
export const getAWinningUsers = (params) => {
	return http.post('/api/records/getAWinningUsers', params)
};

//查询秒杀活动线索客户列表
export const getSeckillClueCustomers = (params) => {
	return http.post('/api/records/getSeckillClueCustomers', params)
};


/**
 * 活动详情
 */
//秒杀活动详情
export const apiActivityInfo = (params) => {
	return http.post('/api/records/apiActivityInfo', params)
};

//同行活动发起客户列表
export const apiCollectInfo = (params) => {
	return http.post('/api/records/apiCollectInfo', params)
};

//同行活动
export const clientPeerInfo = (params) => {
	return http.post('/api/records/clientPeerInfo', params)
};

//抽奖活动页面
export const luckdrawPage = (params) => {
	return http.post('/api/records/luckdrawPage', params)
};

//权益卡详情
export const storeRightsInfo = (params) => {
	return http.post('/api/records/storeRightsInfo', params)
};


/**
 * 客户
 */
//客户列表
export const customerList = (params) => {
	return http.post('/api/staffCustomer/list', params)
};

//客户详情
export const customerDetail = (params) => {
	return http.get('/api/staffCustomer/'+ params)
};

//我的优惠券
export const apiMyCouponList = (params) => {
	return http.post('/api/customer/apiMyCouponList', params)
};

//客户优惠券详情
export const apiMyCouponInfo = (params) => {
	return http.get('/api/customer/apiMyCouponInfo'+ params)
};

//我的订单列表
export const myOrderList = (params) => {
	return http.post('/api/customer/myOrderList', params)
};

//订单统计
export const myOrderCount = (params) => {
	return http.get('/api/customer/myOrderCount/'+ params)
};

//秒杀订单详情
export const seckillOrderInfo = (params) => {
	return http.post('/api/customer/seckillOrderInfo', params)
};
//抽奖订单详情
export const luckdrawOrderInfo = (params) => {
	return http.post('/api/customer/luckdrawOrderInfo', params)
};
//集客订单详情
export const clientCollectActivityOrderInfo = (params) => {
	return http.post('/api/customer/clientCollectActivityOrderInfo', params)
};
//同行活动详情
export const peerOrderInfo = (params) => {
	return http.post('/api/customer/peerOrderInfo', params)
};
//我的权益卡详情
export const clientRightsOrderInfo = (params) => {
	return http.post('/api/customer/clientRightsOrderInfo', params)
};