/**
 * 商品相关api
 */
import {http} from '@/api/service.js'

/**
 * 登陆
 */
export const login = (params) => {
	return http.post('/manage/login', params)
};
/**
 * 登陆
 */
export const sendmsg = (params) => {
	return http.post('/manage/sendmsg', params)
};
/**
 * 切换门店—查询门店集合
 */
export const getStoreList = (params) => {
	return http.get('/manage/getStoreList', params)
};
/**
 * 商品/服务列表
 */
export const goodsserverList = (params) => {
	return http.post('/manage/goodsserver/list', params)
};
/**
 * 商品/服务tab数据
 */
export const goodsserverTabList = (params) => {
	return http.post('/manage/goodsserver/tabList', params)
};
/**
 * 新增商品/服务
 */
export const goodsserverSave = (params) => {
	return http.post('/manage/goodsserver/save', params)
};
/**
 * 修改商品/服务
 */
export const goodsserverEdit = (params) => {
	return http.post('/manage/goodsserver/edit', params)
};
/**
 * 商品/服务详情
 */
export const goodsserverDetail = (params) => {
	return http.post('/manage/goodsserver/detail', params)
};
/**
 * 商品/服务详情
 */
export const serverLabelListFn = (params) => {
	return http.post('/manage/goodsserver/serverLabelList', params)
};
/**
 * 管理端小程序商品/服务修改状态(上下架,删除状态)
 */
export const goodsserverUpdateStatus = (params) => {
	return http.post('/manage/goodsserver/updateStatus', params)
};
/**
 * 选择商品
 */
export const findGoods = (params) => {
	return http.post('/manage/common/findGoods', params)
};
/**
 * 选择门店
 */
export const findStore = (params) => {
	return http.post('/manage/common/findStore', params)
};
/**
 * 选择优惠券
 */
export const findCoupon = (params) => {
	return http.post('/manage/common/findCoupon', params)
};
/**
 * 选择员工列表
 */
export const findStoreUser = (params) => {
	return http.post('/manage/common/findStoreUser', params)
};
/**
 * 文件上传
 */
export const uploadFiles = (params) => {
	return http.post('/upload/files', params)
};
/**
 * 核销页面数据
 */
export const writeoffDetail = (params) => {
	return http.post('/manage/writeoff/detail', params)
};
/**
 * 核销
 */
export const writeoffWriteOff = (params) => {
	return http.post('/manage/writeoff/writeOff', params)
};
/**
 * 核销tab数据
 */
export const writeoffTabList = (params) => {
	return http.post('/manage/writeoff/tabList', params)
};
/**
 * 核销记录
 */
export const writeoffList = (params) => {
	return http.post('/manage/writeoff/list', params)
};
/**
 * 核销记录详情信息
 */
export const writeoffLogInfo = (params) => {
	return http.post('/manage/writeoff/logInfo', params)
};
/**
 * 客户列表
 */
export const customerList = (params) => {
	return http.post('/manage/customer/list', params)
};
/**
 * 客户详情
 */
export const customerDetail = (params) => {
	return http.post('/manage/customer/detail', params)
};

/**
 * 编辑客户信息
 */
export const customerEdit = (params) => {
	return http.post('/manage/customer/edit', params)
};

/**
 * 客户秒杀活动订单tab数据
 */
export const customerSeckillTab = (params) => {
	return http.post('/manage/customer/seckillTab', params)
};

/**
 * 客户秒杀活动订单列表
 */
export const customerSeckillList = (params) => {
	return http.post('/manage/customer/seckillList', params)
};

/**
 * 客户秒杀活动订单详情
 */
export const customerSeckillDetail = (params) => {
	return http.post('/manage/customer/seckillDetail', params)
};
/**
 * 客户集客活动订单详情
 */
export const customerCollectDetail = (params) => {
	return http.post('/manage/customer/collectDetail', params)
};
/**
 * 客户同行活动订单详情
 */
export const customerPeerDetail = (params) => {
	return http.post('/manage/customer/peerDetail', params)
};
/**
 * 客户商城订单tab数据
 */
export const customerShoppingTab = (params) => {
	return http.post('/manage/customer/shoppingTab', params)
};

/**
 * 客户商城订单列表
 */
export const customerShoppingList = (params) => {
	return http.post('/manage/customer/shoppingList', params)
};

/**
 * 客户商城订单详情
 */
export const customerShoppingDetail = (params) => {
	return http.post('/manage/customer/shoppingDetail', params)
};
/**
 * 客户抽奖活动订单tab数据
 */
export const customerLuckdrawTab = (params) => {
	return http.post('/manage/customer/luckdrawTab', params)
};

/**
 * 客户抽奖活动订单列表
 */
export const customerLuckdrawList = (params) => {
	return http.post('/manage/customer/luckdrawList', params)
};

/**
 * 客户抽奖活动订单详情
 */
export const customerLuckdrawDetail = (params) => {
	return http.post('/manage/customer/luckdrawDetail', params)
};

/**
 * 客户权益卡列表
 */
export const customerRightsList = (params) => {
	return http.post('/manage/customer/rightsList', params)
};

/**
 * 客户权益卡详情
 */
export const customerRightsDetail = (params) => {
	return http.post('/manage/customer/rightsDetail', params)
};

/**
 * 客户优惠券列表
 */
export const customerCouponList = (params) => {
	return http.post('/manage/customer/couponList', params)
};

/**
 * 客户优惠券列表
 */
export const customerApplyStoreList = (params) => {
	return http.post('/manage/customer/applyStoreList', params)
};

/**
 * 客户优惠券列表
 */
export const customerApplyGoodsList = (params) => {
	return http.post('/manage/customer/applyGoodsList', params)
};

/**
 * 客户洗车订单Tab统计
 */
export const customerWashCarTab = (params) => {
	return http.post('/manage/customer/washCarTab', params)
};

/**
 * 客户洗车订单列表
 */
export const customerWashCarList = (params) => {
	return http.post('/manage/customer/washCarList', params)
};

/**
 * 客户洗车订单详情
 */
export const customerWashCarDetail = (params) => {
	return http.post('/manage/customer/washCarDetail', params)
};
/**
 * 选择门店展示列表

 */
export const showStoreList = (params) => {
	return http.post('/manage/common/storeList', params)
};
/**
 * 选择商品展示列表
 */
export const showStoreGoodsList = (params) => {
	return http.post('/manage/common/goodsList', params)
};
/**
 * 选择员工展示列表
 */
export const showStoreUserList = (params) => {
	return http.post('/manage/common/storeUserList', params)
};
/**
 * 选择品牌/车型/车系展示列表
 */
export const showBrandSeriesModelList = (params) => {
	return http.post('/manage/common/brandSeriesModelList', params)
};
/**
 * 选择客户展示列表
 */
export const showCustomerList = (params) => {
	return http.post('/manage/common/customerList', params)
};
/**
 * 选择工项展示列表
 */
export const showWorkItemList = (params) => {
	return http.post('/manage/common/workItemList', params)
};
/**
 * 选择客户

 */
export const findCustomer = (params) => {
	return http.post('/manage/common/findCustomer', params)
};

/**
 * 活动订单列表

 */
export const activityOrderTab = (params) => {
	return http.post('/manage/customer/activityOrderTab', params)
};
/**
 * 活动订单列表

 */
export const activityOrderList = (params) => {
	return http.post('/manage/customer/activityOrderList', params)
};
/**
 * 客户秒杀活动订单详情


 */
export const seckillDetail = (params) => {
	return http.post('/manage/customer/seckillDetail', params)
};
/**
 * 客户抽奖活动订单详情

 */
export const luckdrawDetail = (params) => {
	return http.post('/manage/customer/luckdrawDetail', params)
};
/***
 * 员工列表
 */
export const storeuserList = (params) => {
	return http.post('/manage/storeuser/list', params)
};/***
 * 新增员工
 */
export const storeuserSave = (params) => {
	return http.post('/manage/storeuser/save', params)
};/***
 * 修改员工
 */
export const storeuserEdit = (params) => {
	return http.post('/manage/storeuser/edit', params)
};/***
 * 员工详情
 */
export const storeuserDetail = (params) => {
	return http.post('/manage/storeuser/detail', params)
};/***
 * 员工状态修改
 */
export const storeuserUpdateStatus = (params) => {
	return http.post('/manage/storeuser/updateStatus', params)
};/***
 * 员工职务列表
 */
export const storeuserPositionList = (params) => {
		return http.post('/manage/storeuser/positionList', params)
	};
/***
 * 员工模块权限列表
 */
export const storeuserModuleList = (params) => {
	return http.post('/manage/storeuser/moduleList', params)
};
/* 
 * 商品类型列表
*/
export const goodsTypeList = (params) => {
	return http.post('/manage/goodsserver/goodsTypeList', params)
};/*
 * 商品类型列表
*/
export const goodsDataList = (params) => {
	return http.post('/manage/goodsserver/goodsDataList', params)
};/*
 * 商品类型列表
*/
export const goodsData = (params) => {
	return http.post('/manage/goodsserver/goodsData', params)
};