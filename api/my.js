import {
	http
} from '@/api/service.js'
/**
 * 门店详情

 */
export const storeInfo = (params) => {
	return http.post('/manage/my/storeInfo', params)
};
/**
 * 评价列表

 */
export const scoreList = (params) => {
	return http.post('/manage/my/scoreList', params)
};
/**
 * 评价详情

 */
export const scoreDetail = (params) => {
	return http.post('/manage/my/scoreDetail', params)
};
/**
 * 评价详情

 */
export const storeQrCode = (params) => {
	return http.post('/manage/my/storeQrCode', params)
};
/**
 * 门店收益明细

 */
export const incomeDetails = (params) => {
	return http.post('/manage/my/incomeDetails', params)
};
/**
 * 门店收益列表数据

 */
export const incomeList = (params) => {
	return http.post('/manage/my/incomeList', params)
};
/**
 * 门店收益今日统计数据

 */
export const todayIncome = (params) => {
	return http.post('/manage/my/todayIncome', params)
};
/**
 * 门店收益详情数据

 */
export const incomeDetailInfo = (params) => {
	return http.post('/manage/my/incomeDetailInfo', params)
};
/**
 * 隐藏/展示评价

 */
export const updateScoreStatus = (params) => {
	return http.post('/manage/my/updateScoreStatus', params)
};
/**
 * 门店累计总收益

 */
export const incomeCount = (params) => {
	return http.post('/manage/my/incomeCount', params)
};