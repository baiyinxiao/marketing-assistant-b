import {http} from '@/api/service.js'

/**
 * 优惠券列表
 */
export const couponList = (params) => {
    return http.post('/manage/coupon/list', params)
};
/**
 * 新增优惠券
 */
export const couponSave = (params) => {
    return http.post('/manage/coupon/save', params)
};
/**
 * 优惠券详情

 */
export const couponDetail = (params) => {
    return http.post('/manage/coupon/detail', params)
};
/**
 * 优惠券发送指定用户
 */
export const couponSendAssignment = (params) => {
    return http.post('/manage/coupon/sendAssignment', params)
};
/**
 * 优惠券修改状态
 */
export const couponUpdateStatus = (params) => {
    return http.post('/manage/coupon/updateStatus', params)
};
/**
 * 优惠券选择工项
 */
export const couponWorkItem = (params) => {
    return http.post('/manage/coupon/workItem', params)
};
/**
 * 优惠活动数据
 */
export const couponData = (params) => {
    return http.post('/manage/coupon/couponData', params)
};
/**
 * 优惠已领取/已用客户列表
 */
export const couponDataList = (params) => {
    return http.post('/manage/coupon/couponDataList', params)
};
/**
 * 新增权益卡
 */
export const rightsSave = (params) => {
    return http.post('/manage/rights/save', params)
};
/**
 * 查看权益卡
 */
export const rightsDetail = (params) => {
    return http.post('/manage/rights/detail', params)
};
/**
 * 权益卡回显
 */
export const rightsEditEcho = (params) => {
    return http.post('/manage/rights/editEcho', params)
};
/**
 * 编辑权益卡
 */
export const rightsEdit = (params) => {
    return http.post('/manage/rights/edit', params)
};
/**
 * 权益卡列表查看
 */
export const rightsList = (params) => {
    return http.post('/manage/rights/list', params)
};
/**
 * 权益卡预览
 */
export const rightsPreview = (params) => {
    return http.post('/manage/rights/preview', params)
};
/**
 * 权益卡列表查看
 */
export const rightsUpdateStatus = (params) => {
    return http.post('/manage/rights/updateStatus', params)
};
/**
 * 权益卡活动数据
 */
export const rightsData = (params) => {
	return http.post('/manage/rights/rightsData', params)
};
/**
 * 权益卡活动数据____表格
 */
export const rightsDataList = (params) => {
	return http.post('/manage/rights/rightsDataList', params)
};
/**
 * 选择品牌/车型/车系
 */
export const findBrandSeriesModel = (params) => {
    return http.post('/manage/common/findBrandSeriesModel', params)
};
/**
 * 新增抽奖活动
 */
export const luckdrawSave = (params) => {
    return http.post('/manage/luckdraw/save', params)
};
/**
 * 查看抽奖活动
 */
export const luckdrawDetail = (params) => {
    return http.post('/manage/luckdraw/detail', params)
};
/**
 * 抽奖数据回显
 */
export const lotteryEditEcho = (params) => {
    return http.post('/manage/luckdraw/editEcho', params)
};
/**
 * 抽奖编辑
 */
export const lotteryEdit = (params) => {
    return http.post('/manage/luckdraw/edit', params)
};
/**
 * 修改抽奖活动状态
 */
export const luckdrawUpdateStatus = (params) => {
    return http.post('/manage/luckdraw/updateStatus', params)
};
/**
 * 抽奖活动列表查看
 */
export const luckdrawList = (params) => {
    return http.post('/manage/luckdraw/list', params)
};
/**
 * 抽奖活动数据
 */
export const luckdrawData = (params) => {
    return http.post('/manage/luckdraw/luckdrawData', params)
};
/**
 * 抽奖活动数据——表格
 */
export const luckdrawDataList = (params) => {
    return http.post('/manage/luckdraw/luckdrawDataList', params)
};
/**
 * 新增秒杀活动
 */
export const seckillSave = (params) => {
    return http.post('/manage/seckill/save', params)
};
/**
 * 查看秒杀活动详情
 */
export const seckillDetail = (params) => {
    return http.post('/manage/seckill/detail', params)
};
/**
 * 秒杀活动列表查看
 */
export const seckillList = (params) => {
    return http.post('/manage/seckill/list', params)
};
/**
 * 修改秒杀活动状态
 */
export const seckillUpdateStatus = (params) => {
    return http.post('/manage/seckill/updateStatus', params)
};
/**
 * 预览秒杀活动状态
 */
export const seckillPreview = (params) => {
    return http.post('/manage/seckill/preview', params)
};
/**
 * 预览秒杀活动状态
 */
export const seckillEdit = (params) => {
    return http.post('/manage/seckill/edit', params)
};
/**
 * 预览秒杀活动状态
 */
export const seckillEditEcho = (params) => {
    return http.post('/manage/seckill/editEcho', params)
};
/**
 * 秒杀活动数据
 */
export const seckillData = (params) => {
    return http.post('/manage/seckill/seckillData', params)
};
/**
 * 已购买/浏览客户活动数据
 */
export const seckillDataList = (params) => {
    return http.post('/manage/seckill/seckillDataList', params)
};
/**
 * 新增集客活动
 */
export const collectSave = (params) => {
    return http.post('/manage/collect/save', params)
};
/**
 * 修改集客活动
 */
export const collectEdit = (params) => {
    return http.post('/manage/collect/edit', params)
};
/**
 * 集客活动列表
 */
export const collectList = (params) => {
    return http.post('/manage/collect/list', params)
};
/**
 * 修改集客活动状态
 */
export const collectUpdateStatus = (params) => {
    return http.post('/manage/collect/updateStatus', params)
};
/**
 * 集客活动详情
 */
export const collectDetail = (params) => {
    return http.post('/manage/collect/detail', params)
};
/**
 * 集客活动详情
 */
export const collectPreview = (params) => {
    return http.post('/manage/collect/preview', params)
};
/**
 * 新增同行活动
 * @param params
 * @returns {*}
 */
export const peerSave = (params) => {
    return http.post('/manage/peer/save', params)

}
/**
 * 编辑同行活动
 * @param params
 * @returns {*}
 */
export const peerEdit = (params) => {
    return http.post('/manage/peer/edit', params)

}
/**
 * 同行活动数据回显
 * @param params
 * @returns {*}
 */
export const peerEditEcho = (params) => {
    return http.post('/manage/peer/editEcho', params)

}
/**
 * 同行活动详情
 * @param params
 * @returns {*}
 */
export const peerDetail = (params) => {
    return http.post('/manage/peer/detail', params)

}
/**
 * 同行活动列表
 * @param params
 * @returns {*}
 */
export const peerList = (params) => {
    return http.post('/manage/peer/list', params)

}
/**
 * 新增同行活动
 * @param params
 * @returns {*}
 */
export const peerPreview = (params) => {
    return http.post('/manage/peer/preview', params)

}
/**
 * 修改同行活动状态
 * @param params
 * @returns {*}
 */
export const peerUpdateStatus = (params) => {
    return http.post('/manage/peer/updateStatus', params)

}
export const peerData = (params) => {
    return http.post('/manage/peer/peerData', params)

}
export const peerDataList = (params) => {
    return http.post('/manage/peer/peerDataList', params)

}
/**
 * 集客活动详情
 */
export const collectCopy = (params) => {
    return http.post('/manage/collect/copy', params)
};
/**
 * 集客活动详情
 */
export const collectScore = (params) => {
    return http.get('/manage/collect/collectScore', params)
};
export const collectData = (params) => {
    return http.post('/manage/collect/collectData', params)
};
export const collectDataList = (params) => {
    return http.post('/manage/collect/collectDataList', params)
};
