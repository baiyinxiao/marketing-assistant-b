import Vue from 'vue'
const url = {
	// httpsUrl : 'http://10.11.10.129:9528/yxhe-manage-api', /* 治家本地 */
	httpsUrl : 'https://sj.shopec.com.cn/yxhe-manage-api/', /* 根域名不同 */
	imagePath: 'https://sj.shopec.com.cn/image-server/',
}
export default {
	url
}


var filePath = {
	install(Vue) {
		Vue.prototype.$httpUrl = url.httpsUrl
		Vue.prototype.$imagePath = url.imagePath
	},
};

Vue.use(filePath)
