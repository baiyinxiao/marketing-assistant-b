/*
|--------------------------------------------------------------------------
| 全局配置
|--------------------------------------------------------------------------
*/

module.exports = {
	/**
	 * 全局接口地址
	 * @description 
	 */
	baseUrl: 'https://sj.shopec.com.cn/yxhe-client-api/', //正式服务器
	// baseUrl: 'hhttp://119.3.209.214:9527/yxhe-client-api/', //测试服务器
	// baseUrl: 'http://10.11.10.129:9527/yxhe-client-api/', //元宝本地
	// baseUrl: 'http://10.11.10.141:9527/yxhe-client-api/', // 治家本地
	baseUrl2: '', // 服务器2

	/**
	 * 全局图片地址
	 * @description 在全局工具包与全局过滤器中使用。vue过滤器使用文档 https://cn.vuejs.org/v2/guide/filters.html
	 */
	baseImageUrl: 'https://sj.shopec.com.cn/image-server',

	/**
	 * 租户id
	 * @description 目前是写死的，不知道是否需要再程序内修改。如需修改，就要移动到vuex中。
	 */
	tenantId: '22',

	//腾讯定位服务key
	mapKey: 'ZTBBZ-DN365-KMRIU-QJ3JI-7THCE-VZB6F',

	/**
	 * 首页顶部配图
	 */
	homeTopBg: '/dz/db.png',
	homeTopBgVip: '/dz/vip.png',
	/**
	 * 首页引导指南1
	 */
	homeTip1: '/dz/zczn1.png',
	znTitle: '/dz/zhinan_title.png',
	zn101: '/dz/zn1.png',
	zn102: '/dz/zn2.png',
	zn103: '/dz/zn3.png',
	zn104: '/dz/zn4.png',
	zn105: '/dz/zn5.png',
	zn106: '/dz/zn6.png',
	/**
	 * 首页引导指南2
	 */
	homeTip2: '/dz/zczn2.png',
	/**
	 * 首页引导指南3
	 */
	homeTip3: '/dz/zczn3.png',

	/**
	 * 客服电话
	 */
	servicePhone: '13571744786'

};
