/*
 * @Description:
 * @Version: 1.0
 * @Autor: lxd
 * @Date: 2020-05-08 13:50:40
 * @LastEditors: lxd
 * @LastEditTime: 2020-07-20 17:50:33
 */
var dtime = "_deadtime";
/**
 * @param {Object} k name
 * @param {Object} v value
 * @param {Object} t 有效时间
 */
function put(k, v, t) {
  uni.setStorageSync(k, v);
  var seconds = parseInt(t);
  if (seconds > 0) {
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000 + seconds;
    uni.setStorageSync(k + dtime, timestamp + "");
  } else {
    uni.removeStorageSync(k + dtime);
  }
}
/**
 * @param {Object} k name
 * @param {Object} def 默认值 非必填
 */
function get(k, def) {
  var deadtime = parseInt(uni.getStorageSync(k + dtime));
  if (deadtime) {
    if (parseInt(deadtime) < Date.parse(new Date()) / 1000) {
      if (def) {
        return def;
      } else {
        return false;
      }
    }
  }
  var res = uni.getStorageSync(k);
  if (res) {
    return res;
  } else {
    return def;
  }
}

function remove(k) {
  uni.removeStorageSync(k);
  uni.removeStorageSync(k + dtime);
}

function clear() {
  uni.clearStorageSync();
}


export {
  put,
  get,
  remove,
  clear
};
