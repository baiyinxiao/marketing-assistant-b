import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        userInfo: {},
        selectStore: [],
        selectStaff: [],
        selectShop: [],
		selectWorkItem:[],
		selectCar:[],
        selectCoupons: [],
        selectStoreStr:'',
        selectStaffStr: '',
        selectShopStr: '',
        selectCouponsStr: '',
		statusBar : 0,//状态栏高度
		customBar : 0 //导航栏高度

    },
    mutations: {
		
        loginStore(state, res) {
            console.log(res)
            state.userInfo = res
            uni.setStorage({
                key: 'userInfo',
                data: res
            })
        },
        setSelectStore(state, res) {

            state.selectStore = res
        },
        setSelectStaff(state, res) {
            state.selectStaff = res
        },
        setSelectShop(state, res) {
            state.selectShop = res
        },
        setSelectCoupons(state, res) {
            state.selectCoupons = res
        },
		setSelectStoreStr(state, res) {
		    state.selectStoreStr = res
		},
		setSelectStaffStr(state, res) {
		    state.selectStaffStr = res
		},
		setSelectShopStr(state, res) {
		    state.selectShopStr = res
		},
		setSelectCouponsStr(state, res) {
		    state.selectCouponsStr = res
		},
		setSelectWorkItem(state, res) {
		    state.selectWorkItem = res
		},
		setSelectCar(state,res){
			state.selectCar = res
		},
		setStatusBar(state, res) {
			state.statusBar = res;
		},
		setCustomBar(state, res) {
			state.customBar = res;
		}
    },
    actions: {},
    getters: {
        userInfo(state, getters) {
            return state.userInfo
        },
        selectStore(state, getters) {
            return state.selectStore
        },
        selectCoupons(state, getters) {
            return state.selectCoupons
        },
        selectStaff(state, getters) {
            return state.selectStaff
        },
        selectShop(state, getters) {
            return state.selectShop
        },
		selectStoreStr(state, getters) {
		    return state.selectStoreStr
		},
		selectCouponsStr(state, getters) {
		    return state.selectCouponsStr
		},
		selectStaffStr(state, getters) {
		    return state.selectStaffStr
		},
		selectShopStr(state, getters) {
		    return state.selectShopStr

        },
		selectWorkItem(state, getters) {
		    return state.selectWorkItem
		},
		selectCar(state, getters){
			return state.selectCar
		},
		statusBar(state, getters) {
			return state.statusBar
		},
	    customBar(state, getters) {
			return state.customBar
		}
    }
})
export default store
