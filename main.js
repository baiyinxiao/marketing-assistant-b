import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
import store from './store'
import {
	put,
	get,
	clear,
	remove
} from '@/utils/storage/storage.js'
import {
	navigateTo,
	navigateBack,
	switchTab,
	redirectTo,
	reLaunch,
} from '@/utils/navigate/navigate.js'
import jiuaiDebounce  from '@/utils/debounce/debounce.js'
import * as filter from '@/utils/common/filter.js'//引入过滤器


Object.keys(filter).forEach(key => {
  Vue.filter(key, filter[key])
})
// 防抖注册
Vue.prototype.$jiuaiDebounce=jiuaiDebounce;

Vue.prototype.$navigateTo = navigateTo
Vue.prototype.$navigateBack = navigateBack
Vue.prototype.$switchTab = switchTab
Vue.prototype.$redirectTo = redirectTo
Vue.prototype.$reLaunch= reLaunch


Vue.prototype.$put = put
Vue.prototype.$get = get 
Vue.prototype.$clear = clear
Vue.prototype.$remove = remove
Vue.prototype.$store = store


Vue.use(uView);
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
